import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Employee';
  details = [
    {
    Employee_Name:'rahul',
    Employee_Id:'1',
    Employee_Designation:'123'
  },
  {
    Employee_Name:'anand',
    Employee_Id:'2',
    Employee_Designation:'123'
  },
  {
    Employee_Name:'tanmay',
    Employee_Id:'3',
    Employee_Designation:'123'
  }
];
add_detail(new_name:any,new_id:any,new_designation:any) {
  var new_detail:any ={
  Employee_Name: new_name,
  Employee_Id:new_id,
  Employee_Designation:new_designation
  };
  this.details.push(new_detail)
}

delete_detail(emp:any){
  this.details = this.details.filter(t =>t.Employee_Name !== emp.Employee_Name);
}

}


